<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
	"http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:dc="http://purl.org/dc/terms/"
xmlns:foaf="http://xmlns.com/foaf/0.1/"
xmlns:og="http://ogp.me/ns#"
xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
xmlns:sioc="http://rdfs.org/sioc/ns#"
xmlns:sioct="http://rdfs.org/sioc/types#"
xmlns:skos="http://www.w3.org/2004/02/skos/core#"
xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
xmlns:article="http://ogp.me/ns/article#"
xmlns:book="http://ogp.me/ns/book#"
xmlns:profile="http://ogp.me/ns/profile#"
xmlns:video="http://ogp.me/ns/video#"
xmlns:product="http://ogp.me/ns/product#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="X-Frame" http-equiv="X-Frame-Options" content="SAMEORIGIN" />
	<link rel="alternate" type="application/rss+xml" title="SBI GENERAL RSS" href="//www.sbigeneral.in/SBIG/rss.xml" />
	<link rel="shortcut icon" href="//www.sbigeneral.in/SBIG/sites/default/files/fav-sbi.png" type="image/png" />
	<meta name="description" content="Sbi description" />
	<link rel="canonical" href="//www.sbigeneral.in/SBIG/" />
	<link rel="shortlink" href="//www.sbigeneral.in/SBIG/" />
	<meta property="og:site_name" content="SBI GENERAL" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="https://www.sbigeneral.in/SBIG/node" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>SBI GENERAL</title>
	<link href="{{ asset('/public/css/style.css?v=p7fry1') }}" rel="stylesheet">
	<link href="{{ asset('/public/css/font-awesome.css?v=p7fry1') }}" rel="stylesheet">
	<link href="{{ asset('/public/css/responsive.css?v=p7fry1') }}" rel="stylesheet">
	<link href="{{ asset('/public/css/developer.css?v=p7fry1') }}" rel="stylesheet">
	

      <!--[if lt IE 10]>
      <script type="text/javascript" src="/SBIG/sites/all/themes/sbigeneral/js/html5.js"></script>
      <script type="text/javascript" src="/SBIG/sites/all/themes/sbigeneral/js/ie.placeholder.js"></script>
      <![endif]-->
</head>
<body class="html front not-logged-in no-sidebars page-node i18n-en" >
  	<!-- Google Tag Manager -->
  	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KTLFW3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  	<script type="text/javascript">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0];var j=d.createElement(s);var dl=l!='dataLayer'?'&l='+l:'';j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;j.type='text/javascript';j.async=true;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-KTLFW3');</script>
  	<!-- End Google Tag Manager -->
  	<div id="skip-link">
  		<a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  	</div>
  	<div id="sb-site">
  		<header id="header">
  			<div class="wrapper clearfix">

  				<div class="clearfix">
  					<div class="logo">
  						<a href="/SBIG/" title="Home" rel="home" id="logo">
  							<img src="https://www.sbigeneral.in/SBIG/sites/all/themes/sbigeneral/logo.png" alt="Home" />
  						</a>
  					</div>

  				</div>
  			</div>
  		</header>

  		<section id="quick-link" class="quicklink">
  			<div class="wrapper">
  				<div class="region region-important-links">
  					<div id="block-sbi-quote-6" class="block block-sbi-quote">
  						<!-- <div class="content">
  							
                    		<div id="renew_print_policy_section" class="popup tran_renewal_print"></div>
                    		<div href="/SBIG/renew_print_policy" class="use-ajax " title="Reprint SBI Group Personal Accident Policy"><span>Reprint SBI Group Personal Accident Policy</span></div>
	                        	
	                    </div> -->
	                    <div class="content">
  							
                    		<div id="renew_print_policy_section" class="popup tran_renewal_print"></div>
                    		<div class="use-ajax " title="Reprint SBI Group Personal Accident Policy">
						    <div class="row otpWrapper" >
						        <span>Please enter otp sent to your registered mobile number ********{{substr( $mobile, -2)}} </span>
								<div id="otpCode">OTP FOR VERIFICATION [<span>{{$otp}}</span>]</div>
						        <div class ="otpInputDiv">
						        	<a style="font-size: 15px;display: block;margin-left: 145px" href="javascript:void(0)" id="otp_resend">Resend</a>
						        	<input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" value =""  type = "number" maxlength = "4" name="otp" id="otp" >
						        </div>
						        <div id="otpError" >
						        	<p>
						        		
						        	</p>
					        	</div>
					        	<div id="otpSuccess" class="hideblock" style="color:green;font-size: 17px;" >
						        	<p>
						        		
						        	</p>
					        	</div>
						        <div class="submitOtp" >
						        	<button type="button" class="btn btn-primary" id="submitOtp" style="padding: 10px;width: 22%" >Submit</button>
						        	
								<a href="#" id="download_pdf" download ></a>
						        </div>
						    </div>
						</div>
	                        	
	                    </div>
	                </div>
		            </div>
	        </div>
    	</section>

	    <footer id="footer">

	    	<div class="region region-copyright">
	    		<div id="block-custom-common-footersection" class="block block-custom-common">
	    			<div class="content">

	    				<section class="footer-bottom">
	    					<div class="wrapper">© 2011 SBI General Insurance Company Limited. All Copy Rights Reserved <br /> Insurance is subject matter of the solicitation, registration number 144 15/12/2009 | CIN: U66000MH2009PLC190546 <br /> SBI Logo displayed above belongs to State Bank of India and used by SBI General Insurance Co. Ltd. under license <br /><span>Best viewed in IE 10 or Firefox 14+, Chrome 22+ at a screen resolution of 1366 x 900 or higher</span> </div>
	    					<div class="footer-bot-link"><ul><li class="first leaf"><a href="/SBIG/privacy-policy" title="">Privacy Policy</a></li>
	    						<li class="leaf"><a href="/SBIG/website-terms-usage" title="">Terms of Usage</a></li>
	    						<li class="last leaf"><a href="/SBIG/ucc-disclaimer" title="">UCC Disclaimer</a></li>
	    					</ul></div> 
	    				</section>
	    			</div>
	    		</div>
	    	</div>
	    </footer>
	</div>
	<script src="{{ asset('/public/js/jquery.js') }}"></script> 
	<script type="text/javascript">
	$(document).ready(function () {
	$.ajaxSetup({
   		 headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
	});
	$(document).on('click','#submitOtp',function(){
	  	var refId = <?php echo "'".$refId."'"; ?>;
	  	$('#otpError').removeClass('showblock').addClass('hideblock');
	  	var otp = $('#otp').val();	  	
	  	validation = true;
	  	/*if(otp == undefined || otp == ''){
	  		alert('Please enter otp');
	  		validation = false;
	  		return false;
	  	} else if(otp.length != 4){
	  		alert('Please enter valid otp');
	  		validation = false;
	  		return false;	
	  	} else */if(validation){
	  		$.ajax({
			   	url: 'otpVerification',
			   	type: 'POST',
			   	data: {
			      	mobile: {{$mobile}},
			      	otp : (otp),
			      	refId : refId,
			   	},
			   	dataType: 'json',
			   	success: function(data) {
			   		if(!data.status){
						// console.log('test if');
			   			$('#otpError > p').text(data.message);
			   			$('#otpError').removeClass('hideblock green').addClass('showblock red');
			   			
			   		}else{
			   			$('#otpError > p').text(data.message);
			   			$('#otpError').removeClass('showblock red').addClass('hideblock green');
			   			$('#otp_resend').removeClass('showblock').addClass('hideblock');
			   			 //var blob=new Blob([data.url]);
			   			// var link = document.createElement('a');
			           		// link.href = window.URL.createObjectURL(data.url);
			         	 	 //link.download = data.url;
			        		// link.click();
						var oldUrl = $('#download_pdf').attr("href"); // Get current url
					        var newUrl = oldUrl.replace('#', data.url);
						$('#download_pdf').attr('href', newUrl);
						$('#download_pdf')[0].click();
				
			   		}	   		
			   	},	
			   	error: function() {
					$('#info').html('<p>An error has occurred</p>');
			   	}
			});	
	  	}	
	  });
	});
	$(document).on('click','#otp_resend',function(){
		var refId = <?php echo "'".$refId."'"; ?>;
  		$.ajax({
		   	url: 'resendOtp',
		   	type: 'POST',
		   	data: {
		      	refId : refId
		   	},
		   	dataType: 'json',
		   	success: function(data) {
		   		console.log('data resend =',data);
		   		if(data.status){
		   			// alert('inside if');
					$('#otpSuccess > p').text(data.message);
					$('#otpSuccess').removeClass('hideblock').addClass('showblock');
					$('#otpCode > span').text(data.otp);
					if(parseInt(data.count) > 3){
						$('#otp_resend').removeClass('showblock').addClass('hideblock');
					}
		   		}else{
		   			$('#otpSuccess >p').text(data.message);
		   		}
		   	},	
		   	error: function() {
				$('#info').html('<p>An error has occurred</p>');
		   	}
			
	  	
	  });
	});
	</script>
</body>
</html>

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/{name}', 'PDFApiController@pdf');
Route::post('/sendOtp', 'PDFApiController@sendOtp')->middleware('checkHeader');
Route::post('/otpVerification', 'PDFApiController@otpVerification');
Route::post('/resendOtp', 'PDFApiController@resendOtp');

Route::any('{all}', function(){
		echo "Page not found";
		exit();	
	})->where('all', '.*');

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/smsapi', 'SMSApiController@smsApi')->middleware('checkHeader');
Route::post('/sms', 'SMSApiController@smsApiNew')->middleware('checkHeader');
//Route::post('api/login', array('uses' => 'APIAuthController@login','middleware' => ['checkHeader']));


//************** Return invalid url message if route doesn't match *********************//
Route::any('{all}', function(){
		$response['status'] = false;
		$response['error_code'] = 404;
		$response['error_message'] = "Api not found";
	    return Response::json($response);
	})->where('all', '.*');

<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Http\Services\CommonService;
use Response,Validator,Config;

class PDFApiController extends Controller {
    public function __construct(CommonService $commonService){
        $this->commonService = $commonService;
    }
    public function resendOtp(Request $request){
        $refId = $request->post('refId');
        $getDetails = $this->commonService->getDateByRefId($refId)['response'];
        if(!empty($getDetails)){
            $start = new \DateTime($getDetails->otp_time);
            $end = new \DateTime();
            $diff=date_diff($end,$start);
            $diffInMin = $diff->s;
            $otp = $getDetails->otp;
            $mobile = $getDetails->mobile;
            $otpCount = $getDetails->otp_count;
            if($diffInMin <= 10){
                return $this->resend($otp,$refId,$mobile,$otpCount+1);
            }else{
                $randNum = rand(1000,9999);
                return $this->resend($randNum,$refId,$mobile,$otpCount+1,1);
            }
        }

    }
    public function resend($otp,$refId,$mobile,$count,$updateTime = 0){
        $smsBody = str_replace(':otp', $otp, Config::get('smstemplate.OTP_sms_template'));
        $smsParams = array(
                    'mobile'   => $mobile,
                    'smsBody'  => $smsBody 
                );
        $result = $this->commonService->sendSMSApi($smsParams);

        $reqPara = array(
            'mobile' => $mobile,
            'otp' => $otp,
            'refId' => $refId,
            'otpCount' => $count,
            'update' => $updateTime,
            'smsBody'  => $smsBody
        );
        /************* Save log ************/
        $logArray = array(
            'mobile' => $mobile,
            'request' => 'Resend otp |'.json_encode($reqPara)
        );
        $id = $this->commonService->saveLog($logArray);
        session()->put('logId', $id);
        /************* End save log ************/
        $resStatus = $this->commonService->saveOtpdetails($reqPara);
        if($resStatus['status']){
            return array('status' => 1,'message' => 'Otp resent successfully!!','otp' => $otp,'count' => $count);
        }else{
            return array('status' => 0,'message' => 'something is wrong,please try later');
        }
    }
    public function sendOtp($mobile,$refId,$productName,$policyId,$quoteId){
        $rand = rand(1000,9999);
        /*echo "<pre>In send Otp <br>";
        $reqArray = array('t1' => 1,'t2' => 2, 't3' => 3);
        $link = 'your :t1 is fullfied by :t2 and :t3 must have catch';
        preg_match_all('/(?<!\w):\w+/',$link,$matches);
        if(!empty($matches)){
            $match = $matches[0];
            foreach ($match as $key => $value) {
                $value = explode(':', $value)[1];
                if(array_key_exists($value, $reqArray)){
                    if(!empty($reqArray[$value])){
                        $smsBody = str_replace($match, $reqArray, $link);
                    }else{

                       print_r(array('status' => 404,'message' => $value .' value is null'));echo "<br>";
                    }
                }else{
                    print_r(array('status' => 404,'message' => $value.' not exist'));echo "<br>";
                }
            }
        }else{
            $smsTemp = Config::get('smstemplate.OTP_sms_template');
        }
        die;*/
        $smsBody = str_replace(':otp', $rand, Config::get('smstemplate.OTP_sms_template'));
        // send otp here
        $smsParams = array(
                    'mobile'   => $mobile,
                    'smsBody'  => $smsBody 
                );
        
        // $result = $this->commonService->sendSMSApi($smsParams);
        
        
        $reqPara = array(
            'mobile' => $mobile,
            'otp' => $rand,
            'refId' => $refId,
            'smsBody'  => $smsBody,
            'otpCount' => 1
        );
        /* ************ Save log ************/
        $logArray = array(
            'mobile' => $mobile,
            'request' => 'otp parameter |'.json_encode($reqPara)
        );
        // $res
        $id = $this->commonService->saveLog($logArray);
        session()->put('logId', $id);
        /* ************ End Save log ************/
        // save otp in db
        $otpSentStatus = $this->commonService->saveOtpdetails($reqPara);
        
        $response['status'] = ($otpSentStatus['response'])? 1 : 0; 
        $response['otp'] = $rand;
        return $response;
    }
    public function pdf($refId, Request $req){
        $getDetails = $this->commonService->checkExpByRefId($refId)['response'];
        if(!$getDetails){
            echo "Page not found";
            exit();
        }
        $start = new \DateTime($getDetails->created_at);        
        $end = new \DateTime();
        $diff = date_diff($end,$start);
        $diffInMin = $diff->d;
        if($diffInMin > 60){
            echo "Page not found";
            exit();   
        }
        $mobile = $getDetails->mobile;
        $productName = $getDetails->product_name;
        $policyId = $getDetails->policy_id;
        $quoteId = $getDetails->quote_id;
        $smsData = array(
                'mobile'    => $mobile,
                'policyId'  => $policyId,
                'quoteId'   => $quoteId,
                'email'     => $getDetails->email,
                'productName'=> $productName,
                'refId'     => $refId,
            
            );
        
        // Send OTP

        /* ************ Send otp *********** */
        $sendOtp = $this->sendOtp($mobile,$refId,$productName,$policyId,$quoteId);

        if($sendOtp['status']){
            $smsData['otp'] = $sendOtp['otp'];
            return view('sms/otpView', $smsData);
        }else{
            return redirect('app/test/'.$id.'/edit')->withErrors($validator);
        }
    }
    public function otpVerification(Request $req){
        $data = $req->all();
        $refId = $data['refId'];
        /* ************ Validation start *********** */
        $validator = Validator::make($data, [
            'mobile'=> "required", // regex:/(01)[0-9]{9}/
            'otp'   => "required|digits:4",
            'refId' => "required"
        ],[
            'otp.digits' => 'Invalid otp entered',
            'otp.required' => 'Please enter otp',
            'refId.required' => 'something is wrong !!',
            'mobile.required' => 'Mobile number not exist'
        ]);
        if ($validator->fails()){
            $err = $validator->messages()->getMessages();
            if(isset($err['otp']) && !empty($err['otp'])){
                $errMsg = $err['otp'][0];
            }else if(isset($err['mobile']) && !empty($err['mobile'])){
                $errMsg = $err['mobile'][0];
            }else if(isset($err['refId']) && !empty($err['refId'])){
                $errMsg = $err['refId'][0];
            }
            $res = array('status' => 0,'message' => $errMsg);
            return response()->json($res);
        }
        /* ************ Validation end *********** */
        
        /* ************ validate otp *********** */
        $otpValidate = $this->commonService->otpValidate($data)['count'];
            
        
        if($otpValidate){
            //get pdf here and responde url
            
            /* ************ Download pdf *********** */
            // $getPdf = $this->commonService->getPdfUrl($refId);
            if($getPdf['status']){
                $decoded = base64_decode($getPdf['url']['1']);
                $file = 'public/upload/'.$getPdf['url'][0];
                file_put_contents($file, $decoded);
                if(chmod($file, 0777)){
                    $res =  array('status' => 1,'message' => 'Otp verified successfully', 'url'=>$file);
                }else{
                    return '';
                }
                
                /* ************ End download pdf *********** */
                /* ************ Save log *********** */
                $reqPara = array(
                    'mobile' => $data['mobile'],
                    'otpVerified' => $otpValidate,
                    'refId' => $data['refId'],
                    'pdfFile'  => $getPdf['url'][0],
                    'download' => 1
                );
                $logArray = array(
                    'mobile' => $data['mobile'],
                    'response' => 'pdf response |'.json_encode($reqPara)
                );
                // $res
                if(session()->has('logId')){
                    $logArray['id'] = session()->get('logId');
                    
                    $this->commonService->updateLog($logArray);
                }else{
                    $this->commonService->saveLog($logArray);
                }
                /* ************ End save log ************/
            }else{
                $res = array('status' => 0,'message' => 'Pdf not found');
            }

            return response()->json($res);
        }else{
            $res = array('status' => 0,'message' => 'Wrong otp !!');
            return response()->json($res);
        }
    }
}

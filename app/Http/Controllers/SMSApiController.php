<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Response, DB, Config;
use App\Http\Services\CommonService;
class SMSApiController extends Controller
{
	   
       public function smsApiNew(Request $request, CommonService $common){
        try{
                $input = $request->all();
                $response = [];
		$matches = [];
		$replaceByVar = [];
		$ref_id = '';
                //********************* validation on request input *****************
                $isValidationError = false;
                if(!isset($input['mobile'])){
                        $response['status'] = false;
                        $response['error_code'] = 400;
                        $response['error_message'] = "mobile parameter missing";
                        $isValidationError = true;
                }elseif (strlen($input['mobile']) < 10 || strlen($input['mobile']) > 12) { 
			# code...
                        $response['status'] = false;
                        $response['error_code'] = 400;
                        $response['error_message'] = "mobile number is invalid";
                        $isValidationError = true;
                }elseif(!isset($input['flag'])){
                        $response['status'] = false;
                        $response['error_code'] = 400;
                        $response['error_message'] = "flag parameter is missing";
                        $isValidationError = true;
                }elseif($input['flag'] < 0 || $input['flag'] > 1){
                        $response['status'] = false;
                        $response['error_code'] = 400;
                        $response['error_message'] = "flag value should be 0 or 1";
                        $isValidationError = true;
                }elseif(!isset($input['source'])){
                        $response['status'] = false;
                        $response['error_code'] = 400;
                        $response['error_message'] = "source parameter is missing";
                        $isValidationError = true;
                }elseif(!isset($input['template_id'])){
                        $response['status'] = false;
                        $response['error_code'] = 400;
                        $response['error_message'] = "template_id  parameter is missing";
                        $isValidationError = true;
                }else{
			$template = Config::get('newsmstemplate.template_id.'.$input['template_id']);
			$regex = '~(:\w+)~';
			$ref_id_status = true;
                        while($ref_id_status){
                            $ref_id = $common->generateRandomString();
                            $chkExistofRefId = DB::table('sms_details')->where('reference_id', $ref_id)->count();
                            if($chkExistofRefId > 0){
                                $ref_id_status = true;
                	        }else{
                                $ref_id_status = false;
         	            }
 	                }

			if (preg_match_all($regex, $template, $matches, PREG_PATTERN_ORDER)) {
	 	  	   foreach ($matches[1] as $key=>$word) {
				$var = ltrim($word, ':');
				if($var == 'link' && $input['flag'] != 0){
					$response['status'] = false;
                                        $response['error_code'] = 400;
                                        $response['error_message'] = "You are passing wrong template_id with flag 1";
                                        $isValidationError = true;
                                        return Response::json($response);	      				
				}elseif($var == 'link' && $input['flag'] == 0){
					$link = url('/')."/".$ref_id;
					$replaceByVar[$key] = $link;
					continue;
				}elseif(!isset($input[$var])){
					$response['status'] = false;
		                        $response['error_code'] = 400;
                		        $response['error_message'] = $var." parameter is missing";
		                        $isValidationError = true;
		                        return Response::json($response);
				}elseif(isset($input[$var])){
					$replaceByVar[$key] = $input[$var];
				}
   			   }
			}
		}
                if($isValidationError){
                        return Response::json($response);
                }
                //********************* validation end ******************************
                //*********** maintain log request ************
		$logId = DB::table('sms_log')
                                        ->insertGetId(['mobile'=>$input['mobile'], 'request'=>json_encode($input)]);
                //********** end log request ***************

                $smsBody = '';
                $insertData = [
                                                'mobile'                => $input['mobile'],
                                                'reference_id'  	=> $ref_id,
                                                'email'                 => isset($input['email'])? $input['email']:'',
                                                'source'                => $input['source'],
                                                'template_id'           => $input['template_id']
				];
		$replaceVar = $matches[1];
                $smsBody = str_replace($replaceVar, $replaceByVar, Config::get('newsmstemplate.template_id.'.$input['template_id']));
		$insertData['sms_text'] = $smsBody;
		//print_r($replaceVar);
                //print_r($replaceByVar);
		//print_r($smsBody);die;
                if($input['flag'] == 0){ // send sms with link to downlad a pdf
                        $insertData['policy_id'] = isset($input['policy_id'])?$input['policy_id']:'';
			$insertData['quote_id'] = isset($input['quote_id'])?$input['quote_id']:'';
			$insertData['product_name'] = (isset($input['product']))?$input['product']:'';
		}
		try{
                	$smsId = DB::table('sms_details')->insertGetId($insertData);
		}catch(Exception $e){
			$response['code'] = 500;
	                $response['message'] = "Interval server error";
			return Response::json($response);
		}
                        $reqArray = [
                                        'mobile'                => $input['mobile'],
                                        'smsBody'               => $smsBody
                        ];
                        $result = $common->sendSMSApi($reqArray);
                        //************* Maintain Log Response ***************************
                        DB::table('sms_log')->where('id', $logId)->update(['response'=>'CURL:-> '.json_encode($result)]);
                        //************* END **********************************

                        if($result){
                                $response['status'] = true;
                    $response['code'] = 200;
                    $response['message'] = "Message sent successfully";
                    DB::table('sms_details')->where('id', $smsId)->update(['is_sms_sent'=>1]);
                        }else{
                                $response['status'] = false;
                    $response['error_code'] = 400;
                    $response['error_message'] = "Message is not sent.";
                        }
                        //************* Maintain Log ***************************
                        DB::table('sms_log')->insert(['mobile'=>$input['mobile'], 'request'=>json_encode($input), 'response'=>json_encode($response)]);
                        //************* END **********************************
                return Response::json($response);
            }catch(Exception $e){
		 $response['status'] = false;
            $response['error_code'] = 500;
            $response['error_message'] = "Internal server error";
            //************* Maintain Log ***************************
                        DB::table('sms_log')->insert(['mobile'=>$input['mobile'], 'request'=>json_encode($input), 'response'=>json_encode($response)]);
                        //************* END **********************************
            return Response::json($response);
            }
       }


       public function smsApi(Request $request, CommonService $common){
    	try{
	    	$input = $request->all();
	    	$response = [];
	    	//********************* validation on request input *****************
	    	$isValidationError = false;
	    	if(!isset($input['policy_id']) && !isset($input['quote_id'])){
	    		$response['status'] = false;
	    		$response['error_code'] = 400;
	    		$response['error_message'] = "policy_id or quote_id parameter missing";
	    		$isValidationError = true;
	    	}
	    	elseif(isset($input['quote_id']) && !isset($input['expiry_date'])){
			$response['status'] = false;
	    		$response['error_code'] = 400;
	    		$response['error_message'] = "expiry_date parameter missing";
	    		$isValidationError = true;

	    	}
	    	elseif(!isset($input['mobile'])){
	    		$response['status'] = false;
	    		$response['error_code'] = 400;
	    		$response['error_message'] = "mobile parameter missing";
	    		$isValidationError = true;
	    	}elseif (strlen($input['mobile']) < 10 || strlen($input['mobile']) > 12) {
	    		# code...
	    		$response['status'] = false;
	    		$response['error_code'] = 400;
	    		$response['error_message'] = "mobile number is invalid";
	    		$isValidationError = true;
	    	}elseif(!isset($input['product'])){
	    		$response['status'] = false;
	    		$response['error_code'] = 400;
	    		$response['error_message'] = "product parameter is missing";
	    		$isValidationError = true;
	    	}elseif(!isset($input['flag'])){
	    		$response['status'] = false;
	    		$response['error_code'] = 400;
	    		$response['error_message'] = "flag parameter is missing";
	    		$isValidationError = true;
	    	}elseif($input['flag'] < 0 || $input['flag'] > 1){
	    		$response['status'] = false;
	    		$response['error_code'] = 400;
	    		$response['error_message'] = "flag value should be 0 or 1";
	    		$isValidationError = true;
	    	}elseif(!isset($input['source'])){
			$response['status'] = false;
                        $response['error_code'] = 400;
                        $response['error_message'] = "source parameter is missing";
                        $isValidationError = true;
		}elseif(!isset($input['template_id'])){
                        $response['status'] = false;
                        $response['error_code'] = 400;
                        $response['error_message'] = "template_id  parameter is missing";
                        $isValidationError = true;
                }
	    	if($isValidationError){
	    		return Response::json($response);
	    	}
	    	//********************* validation end ******************************
	    	//*********** maintain log request ************
	    	$logId = DB::table('sms_log')
	    				->insertGetId(['mobile'=>$input['mobile'], 'request'=>json_encode($input)]);   	
	    	//********** end log request ***************
	
	    	$smsBody = '';
	    	$ref_id = '';
	    	$ref_id_status = true;
	    	while($ref_id_status){
	    		$ref_id = $common->generateRandomString();
	    		$chkExistofRefId = DB::table('sms_details')->where('reference_id', $ref_id)->count();
	    		if($chkExistofRefId > 0){
	    			$ref_id_status = true;
	    		}else{
	    			$ref_id_status = false;
	    		}
	    	}
	    	$insertData = [
	    					'mobile'		=> $input['mobile'],
	    					'reference_id'	=> $ref_id,
	    					'email'			=> isset($input['email'])? $input['email']:'',
	    					'product_name'		=> $input['product'],
						'source'		=> $input['source'],
						'template_id'		=> $input['template_id']
	    				  ];
		$premium = isset($input['premium'])?$input['premium']:'';
		$issue_date = isset($input['issue_date'])?$input['issue_date']:'';
	    	if($input['flag'] == 0){ // send sms with link to downlad a pdf	    		
		    	$link = url('/')."/".$ref_id;
		    	if(isset($input['policy_id'])){
		    		$replaceVar = [':policy_id', ':link', ':premium', ':product_name', ':issue_date'];
		    		$replaceVarBy = [$input['policy_id'], $link, $premium, $input['product'], $issue_date];
		    		$smsBody = str_replace($replaceVar, $replaceVarBy, Config::get('smstemplate.policy_sms_template.with_link.'.$input['template_id']));
		    		$insertData['policy_id'] = $input['policy_id'];
		    		$insertData['sms_text'] = $smsBody;
		    	}elseif(isset($input['quote_id'])){
		    		$replaceVar = [':quote_id', ':link', ':expiry_date', ':premium'];
		    		$replaceVarBy = [$input['quote_id'], $link, $input['expiry_date'], $premium];
		    		$smsBody = str_replace($replaceVar, $replaceVarBy, Config::get('smstemplate.quote_sms_template.with_link.'.$input['template_id']));
		    		$insertData['quote_id'] = $input['quote_id'];
		    		$insertData['sms_text'] = $smsBody;
		    	}

	    	}elseif($input['flag'] == 1){ // send sms without link for pdf
		    	if(isset($input['policy_id'])){
		    		$replaceVar = [':policy_id', ':premium', ':product_name', ':issue_date'];
		    		$replaceVarBy = [$input['policy_id'], $premium, $input['product'], $issue_date];
		    		$smsBody = str_replace($replaceVar, $replaceVarBy, Config::get('smstemplate.policy_sms_template.without_link.'.$input['template_id']));
		    		$insertData['policy_id'] = $input['policy_id'];
		    		$insertData['sms_text'] = $smsBody;
		    	}elseif(isset($input['quote_id'])){
		    		$replaceVar = [':quote_id', ':expiry_date', ':premium'];
		    		$replaceVarBy = [$input['quote_id'], $input['expiry_date'], $premium];
		    		$smsBody = str_replace($replaceVar, $replaceVarBy, Config::get('smstemplate.quote_sms_template.without_link.'.$input['template_id']));
		    		$insertData['quote_id'] = $input['quote_id'];
		    		$insertData['sms_text'] = $smsBody;
		    	}
	    	}
	    	
	    	$smsId = DB::table('sms_details')->insertGetId($insertData);
			$reqArray = [
					'mobile'		=> $input['mobile'],
					'smsBody'		=> $smsBody 
			];
			$result = $common->sendSMSApi($reqArray);   	
			//************* Maintain Log Response ***************************
			DB::table('sms_log')->where('id', $logId)->update(['response'=>'CURL:-> '.json_encode($result)]);
			//************* END **********************************

			if($result){
				$response['status'] = true;
	            $response['code'] = 200;
	            $response['message'] = "Message sent successfully";
	            DB::table('sms_details')->where('id', $smsId)->update(['is_sms_sent'=>1]);
			}else{
				$response['status'] = false;
	            $response['error_code'] = 400;
	            $response['error_message'] = "Message is not sent.";
			}
			//************* Maintain Log ***************************
			DB::table('sms_log')->insert(['mobile'=>$input['mobile'], 'request'=>json_encode($input), 'response'=>json_encode($response)]);
			//************* END **********************************
	        return Response::json($response);  
	    }catch(Exception $e){
	    	$response['status'] = false;
            $response['error_code'] = 500;
            $response['error_message'] = "Internal server error";
            //************* Maintain Log ***************************
			DB::table('sms_log')->insert(['mobile'=>$input['mobile'], 'request'=>json_encode($input), 'response'=>json_encode($response)]);
			//************* END **********************************
            return Response::json($response);
	    }
    }
    
}

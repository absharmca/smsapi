<?php

namespace App\Http\Middleware;

use Closure, Config, Response;

class checkHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset($_SERVER['HTTP_X_APPKEY'])){  
            $response['status'] = false;
            $response['error_code'] = 401;
            $response['error_message'] = "Unauthorized user";
            return Response::json($response);  
        }  
  
        if($_SERVER['HTTP_X_APPKEY'] != Config::get('apiconfig.X-APPKEY')){  
            $response['status'] = false;
            $response['error_code'] = 401;
            $response['error_message'] = "Unauthorized user";
            return Response::json($response);  
        }   
        return $next($request);
    }
}

<?php

namespace App\Http\Services;

use Illuminate\Http\Request;

use Response,DB,DateTime;

class CommonService {
    public function sendSMSApi($reqArray){
        try{
       // $smsApi = "https://103.17.18.60/SMS_Notification/Services/PS_SMSPush_V1.0?wsdl";           
    $smsApi = "http://172.18.115.32:7001/SMS_Notification/Services/PS_SMSPush_V1.0?wsdl";
    $client = new \SoapClient($smsApi, array('soap_version'=>SOAP_1_1, 'trace'=>true));
    $header = [
            'Sender'    => 'PORTAL',
            'Receiver'  => 'SOA',
            'DateTime'  => date('Y-m-d H:i:s'),
            'TransactionID' => '',
            'CustomerID'    => '',
            'PartyID'   => '',
            'UserID'    => '60039',
            'TaskID'    =>''
        ];
    $request = [
        'MessageId' => '1063',
        'PhoneNumber'   => $reqArray["mobile"],
        'MessageText'   => $reqArray['smsBody'],
        'CategoryCD'    => 1,
        'IS_OTP'    => 'Y'
        ];
    $params = [
        'Header' => $header,
        'Request'=> $request    
        ];
    $res = $client->process( $params);
        return true;
        }catch(SoapFault $e){
            return false;
        } 
    }
 
    public function CURL($url, $params = array()){
        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $params );
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        }catch(Exception $e){
            return false;
        }
    }
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function getDateByRefId($refId){
        try{
            $date = \Carbon\Carbon::today()->subDays(5);
            $result = DB::table('sms_details')
                    ->where([['reference_id','=', $refId]])
                    // ->where('created_at', '>=', $date)
                    // ->whereBetween('created_at', [$fromDate, $toDate])
                    ->select('mobile','policy_id','quote_id','email','product_name','otp','otp_count','otp_time')
                    ->first();
            return array('status' => 1,'response' => $result);
        } catch (Exception $e){
            return array('status' => 0,'response' => $e->getMessage());
        }
    }
    public function checkExpByRefId($refId){
        try{
            $date = \Carbon\Carbon::today()->subDays(5);
            $result = DB::table('sms_details')
                    ->where([['reference_id','=', $refId]])
                    ->where('created_at', '>=', $date)
                    // ->whereBetween('created_at', [$fromDate, $toDate])
                    ->select('mobile','policy_id','quote_id','email','product_name','otp','created_at')
                    ->first();
            return array('status' => 1,'response' => $result);
        } catch (Exception $e){
            return array('status' => 0,'response' => $e->getMessage());
        }
    }
    public function saveOtpdetails($details){
        try{
            $updateArray = [
                'otp' => $details['otp'],
                'is_otp_sent' => 1,
                'is_otp_verified' => 0,
                'otp_count' => $details['otpCount']                
            ];
            if(!isset($details['update']) || $details['update']){
                    $updateArray['otp_time'] = new \DateTime();
            }
            $result = DB::table('sms_details')
                    ->where([['reference_id','=', $details['refId']],['mobile', '=', $details['mobile']]])
                    ->update($updateArray);
            
            return array('status' => 1,'response' => $result);
        } catch (Exception $e){
            return array('status' => 0,'response' => $e->getMessage());
        }
    }
    public function otpValidate($data){
            $count = DB::table('sms_details')
                    ->select(DB::raw('COUNT(id) as count'))
                    ->where([
                                ['mobile', '=', (string)$data['mobile']],
                                ['reference_id','=', $data['refId']],
                                ['otp','=', (string)$data['otp']]
                            ])
                    ->get();

         DB::table('sms_details')
                    ->where([
                                ['mobile', '=', (string)$data['mobile']],
                                ['reference_id','=', $data['refId']],
                                ['otp','=', (string)$data['otp']]
                            ])
                    ->update(['is_otp_verified' => 1]);
        return array('count' => $count[0]->count);            // ->get();
    }
    public function getPdfUrl($refId){
        $getDetails = DB::table('sms_details')
                    ->where([['reference_id','=', $refId]])
                    ->select('mobile','policy_id','quote_id','email','product_name','otp')
                    ->first();
                    // $this->getDateByRefId($refId)['response'];
        $strType = $strAttr = $strValue = '';
        if($getDetails->policy_id != Null){
            $strType = 'Policy';
            $strAttr = 'Policyid';
            $strValue = $getDetails->policy_id;
        }else{
            $strType = 'Quote';
            $strAttr = 'Quoteid';
            $strValue = $getDetails->quote_id;
        }
        
        // get docId from above $docApi
        $docApi = $this->docApi($strType,$strAttr,$strValue);
//print_r($docApi);die; 
    if($docApi['status']){
            // get pdf url
        $pdfApi = $this->pdfApi($docApi['docId']);
        //print_r($pdfApi);die;
            return $pdfApi = $this->pdfApi($docApi['docId']);
    }else{
        return array('status' => 0,'message' => 'Pdf not generated');
    }

    }
    public function docApi($strType,$strAttr,$strValue){
        $docWsdl = 'http://172.18.115.32:7001/Documentum/PS_DocSearch_V1.0?wsdl';
     $client = new \SoapClient($docWsdl, array('soap_version'=>SOAP_1_1, 'trace'=>true));
        $header = [
                        'Sender'        => 'soa',
                        'Receiver'      => 'soa',
                        'UserID'        => '60034'
                ];
        $request = [
                'StrTypeName'     => $strType,
                'StrAttrName'   => $strAttr,
                'StrValue'   => $strValue                
                ];
        $params = [
                'Header' => $header,
                'Request'=> $request
                ];
    $res = $client->documentumsearchRequestCreation($params);
//  print_r($res);die;
    $url = $res->Response->URL;
    if($url != 'No Results found for the given Criteria'){
        $docRes = explode(';',$url)[1];
        $docId = explode('=',$docRes);      
        return  array('status' => 1,'docId' => $docId[1]);
    }else{
        return array('status' => 0,'message' => 'Pdf not generated');
    }     
    }
    public function pdfApi($docId){
        $pdfWsdl = 'http://172.18.115.32:7001/Documentum/PS_DocSearch_V1.0?wsdl';
//   $docWsdl = 'http://172.18.115.32:7001/Documentum/PS_DocSearch_V1.0?wsdl';
         $client = new \SoapClient($pdfWsdl, array('soap_version'=>SOAP_1_1, 'trace'=>true));
        $header = [
                        'Sender'        => 'soa',
                        'Receiver'      => 'soa',
                        'UserID'        => '60034'
                ];
        $request = [
                'StrTypeName'     => 'portal_doc',
                'StrAttrName'   => 'DocumentId',
                'StrValue'   => $docId
                ];
        $params = [
                'Header' => $header,
                'Request'=> $request
                ];
        $res = $client->documentumsearchRequestCreation($params);
//  print_r($res);
        $url = $res->Response->URL;
        if($url != 'No Results found for the given Criteria'){
        return array('status' => 1,'url' => $url);
     }
    }
    public function saveLog($params){
        return DB::table('sms_log')
                        ->insertGetId($params);
    }
    public function updateLog($params){
        $id = $params['id'];
        unset($params['id']);
        return DB::table('sms_log')
                    ->update($params)
                    ->where('id','=',$id);   
    }
}

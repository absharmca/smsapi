-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2018 at 07:23 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `smsapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `sms_details`
--

CREATE TABLE `sms_details` (
  `id` int(11) NOT NULL,
  `policy_id` varchar(30) DEFAULT NULL,
  `quote_id` varchar(255) DEFAULT NULL,
  `mobile` varchar(15) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `reference_id` varchar(10) NOT NULL,
  `sms_text` text NOT NULL,
  `is_sms_sent` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=> NO, 1=> YES',
  `otp` varchar(10) DEFAULT NULL,
  `is_otp_sent` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=> NO, 1=> YES',
  `is_otp_verified` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=>NO, 1=YES',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='maintain sms and otp';

--
-- Truncate table before insert `sms_details`
--

TRUNCATE TABLE `sms_details`;
--
-- Dumping data for table `sms_details`
--

INSERT INTO `sms_details` (`id`, `policy_id`, `quote_id`, `mobile`, `email`, `reference_id`, `sms_text`, `is_sms_sent`, `otp`, `is_otp_sent`, `is_otp_verified`, `created_at`, `updated_at`) VALUES
(1, '128992', NULL, '7352988879', NULL, '9KMhmlCoD7', 'Dear customer, your policy 128992 is approve. please download pdf from http://192.168.43.17/9KMhmlCoD7', 0, NULL, 0, 0, '2018-05-09 10:12:59', '2018-05-09 10:12:59'),
(2, NULL, '1233498-09', '7352988879', NULL, 'BlmzbGWqtq', 'Dear customer, your quote 1233498-09 is approve. please download pdf from http://192.168.43.17/BlmzbGWqtq', 0, NULL, 0, 0, '2018-05-09 10:47:47', '2018-05-09 10:47:47'),
(3, NULL, '1233498-09', '7352988879', NULL, 'I0x1rcLCG5', 'Dear customer, your quote 1233498-09 is approve. please download pdf from http://192.168.43.17/I0x1rcLCG5', 0, NULL, 0, 0, '2018-05-09 10:59:54', '2018-05-09 10:59:54'),
(4, NULL, '1233498-09', '7352988879', '', 'Dvw0mhMoFB', 'Dear customer, your quote 1233498-09 is approve.', 0, NULL, 0, 0, '2018-05-09 12:42:49', '2018-05-09 12:42:49'),
(5, NULL, '1233498-09', '7352988879', 'absharmca786@gmail.com', 'hPH8o0rT04', 'Dear customer, your quote 1233498-09 is approve. please download pdf from http://192.168.43.17/hPH8o0rT04', 0, NULL, 0, 0, '2018-05-09 12:43:53', '2018-05-09 12:43:53'),
(6, NULL, '1233498-09', '7352988879', 'absharmca786@gmail.com', 'xwwP22K6iQ', 'Dear customer, your quote 1233498-09 is approve. please download pdf from http://192.168.43.17/xwwP22K6iQ', 0, NULL, 0, 0, '2018-05-09 12:57:24', '2018-05-09 12:57:24'),
(7, '328539439132919', NULL, '7352988879', 'absharmca786@gmail.com', 'UQ3KPsiEyd', 'Dear customer, your policy 328539439132919 is approve. please download pdf from http://192.168.43.17/UQ3KPsiEyd', 0, NULL, 0, 0, '2018-05-09 12:58:59', '2018-05-09 12:58:59');

-- --------------------------------------------------------

--
-- Table structure for table `sms_log`
--

CREATE TABLE `sms_log` (
  `id` int(11) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `request` text NOT NULL,
  `response` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `sms_log`
--

TRUNCATE TABLE `sms_log`;
--
-- Dumping data for table `sms_log`
--

INSERT INTO `sms_log` (`id`, `mobile`, `request`, `response`, `created_at`, `updated_at`) VALUES
(1, '7352988879', '{\"policy\":\"128992\",\"quote\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', NULL, '2018-05-08 12:40:01', '2018-05-08 12:40:01'),
(2, '7352988879', '{\"policy\":\"128992\",\"quote\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', 'false', '2018-05-08 12:41:09', '2018-05-08 12:41:30'),
(3, '7352988879', '{\"policy\":\"128992\",\"quote\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', 'false', '2018-05-08 12:43:22', '2018-05-08 12:43:43'),
(4, '7352988879', '{\"policy\":\"128992\",\"quote\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', '\"abshar\"', '2018-05-08 12:44:33', '2018-05-08 12:44:54'),
(5, '7352988879', '{\"policy\":\"128992\",\"quote\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', 'false', '2018-05-08 12:49:08', '2018-05-08 12:49:29'),
(6, '7352988879', '{\"policy\":\"128992\",\"quote\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', '{\"status\":false,\"error_code\":400,\"error_message\":\"Message is not sent.\"}', '2018-05-08 12:49:29', '2018-05-08 12:49:29'),
(7, '7352988879', '{\"policy\":\"128992\",\"quote\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', 'CURL:-> false', '2018-05-08 12:50:22', '2018-05-08 12:50:43'),
(8, '7352988879', '{\"policy\":\"128992\",\"quote\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', '{\"status\":false,\"error_code\":400,\"error_message\":\"Message is not sent.\"}', '2018-05-08 12:50:43', '2018-05-08 12:50:43'),
(9, '7352988879', '{\"policy\":\"128992\",\"quote\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', 'CURL:-> false', '2018-05-08 13:38:10', '2018-05-08 13:38:32'),
(10, '7352988879', '{\"policy\":\"128992\",\"quote\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', '{\"status\":false,\"error_code\":400,\"error_message\":\"Message is not sent.\"}', '2018-05-08 13:38:32', '2018-05-08 13:38:32'),
(11, '7352988879', '{\"policy\":\"128992\",\"quote\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', 'CURL:-> false', '2018-05-09 05:10:57', '2018-05-09 05:11:19'),
(12, '7352988879', '{\"policy\":\"128992\",\"quote\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', '{\"status\":false,\"error_code\":400,\"error_message\":\"Message is not sent.\"}', '2018-05-09 05:11:19', '2018-05-09 05:11:19'),
(13, '7352988879', '{\"policy\":\"128992\",\"quote\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', NULL, '2018-05-09 10:09:58', '2018-05-09 10:09:58'),
(14, '7352988879', '{\"policy_id\":\"128992\",\"quote_id\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', NULL, '2018-05-09 10:10:43', '2018-05-09 10:10:43'),
(15, '7352988879', '{\"policy_id\":\"128992\",\"quote_id\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', NULL, '2018-05-09 10:12:36', '2018-05-09 10:12:36'),
(16, '7352988879', '{\"policy_id\":\"128992\",\"quote_id\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', 'CURL:-> false', '2018-05-09 10:12:59', '2018-05-09 10:13:20'),
(17, '7352988879', '{\"policy_id\":\"128992\",\"quote_id\":\"abshar\",\"mobile\":\"7352988879\",\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\"}', '{\"status\":false,\"error_code\":400,\"error_message\":\"Message is not sent.\"}', '2018-05-09 10:13:20', '2018-05-09 10:13:20'),
(18, '7352988879', '{\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\",\"quote_id\":\"1233498-09\",\"mobile\":\"7352988879\"}', 'CURL:-> false', '2018-05-09 10:47:47', '2018-05-09 10:48:09'),
(19, '7352988879', '{\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\",\"quote_id\":\"1233498-09\",\"mobile\":\"7352988879\"}', '{\"status\":false,\"error_code\":400,\"error_message\":\"Message is not sent.\"}', '2018-05-09 10:48:09', '2018-05-09 10:48:09'),
(20, '7352988879', '{\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\",\"quote_id\":\"1233498-09\",\"mobile\":\"7352988879\"}', 'CURL:-> false', '2018-05-09 10:59:54', '2018-05-09 11:00:16'),
(21, '7352988879', '{\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\",\"quote_id\":\"1233498-09\",\"mobile\":\"7352988879\"}', '{\"status\":false,\"error_code\":400,\"error_message\":\"Message is not sent.\"}', '2018-05-09 11:00:16', '2018-05-09 11:00:16'),
(22, '7352988879', '{\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\",\"quote_id\":\"1233498-09\",\"mobile\":\"7352988879\",\"flag\":\"7\"}', NULL, '2018-05-09 12:41:32', '2018-05-09 12:41:32'),
(23, '7352988879', '{\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\",\"quote_id\":\"1233498-09\",\"mobile\":\"7352988879\",\"flag\":\"1\"}', 'CURL:-> false', '2018-05-09 12:42:49', '2018-05-09 12:42:49'),
(24, '7352988879', '{\"product\":\"SBIG life\",\"premium\":null,\"email\":null,\"expiry_date\":\"15-01-2019\",\"quote_id\":\"1233498-09\",\"mobile\":\"7352988879\",\"flag\":\"1\"}', '{\"status\":false,\"error_code\":400,\"error_message\":\"Message is not sent.\"}', '2018-05-09 12:42:49', '2018-05-09 12:42:49'),
(25, '7352988879', '{\"product\":\"SBIG life\",\"premium\":null,\"email\":\"absharmca786@gmail.com\",\"expiry_date\":\"15-01-2019\",\"quote_id\":\"1233498-09\",\"mobile\":\"7352988879\",\"flag\":\"0\"}', 'CURL:-> false', '2018-05-09 12:43:53', '2018-05-09 12:43:53'),
(26, '7352988879', '{\"product\":\"SBIG life\",\"premium\":null,\"email\":\"absharmca786@gmail.com\",\"expiry_date\":\"15-01-2019\",\"quote_id\":\"1233498-09\",\"mobile\":\"7352988879\",\"flag\":\"0\"}', '{\"status\":false,\"error_code\":400,\"error_message\":\"Message is not sent.\"}', '2018-05-09 12:43:53', '2018-05-09 12:43:53'),
(27, '7352988879', '{\"product\":\"SBIG life\",\"premium\":null,\"email\":\"absharmca786@gmail.com\",\"expiry_date\":\"15-01-2019\",\"quote_id\":\"1233498-09\",\"mobile\":\"7352988879\",\"flag\":\"0\"}', 'CURL:-> false', '2018-05-09 12:57:24', '2018-05-09 12:57:24'),
(28, '7352988879', '{\"product\":\"SBIG life\",\"premium\":null,\"email\":\"absharmca786@gmail.com\",\"expiry_date\":\"15-01-2019\",\"quote_id\":\"1233498-09\",\"mobile\":\"7352988879\",\"flag\":\"0\"}', '{\"status\":false,\"error_code\":400,\"error_message\":\"Message is not sent.\"}', '2018-05-09 12:57:24', '2018-05-09 12:57:24'),
(29, '7352988879', '{\"product\":\"SBIG life\",\"premium\":null,\"email\":\"absharmca786@gmail.com\",\"expiry_date\":null,\"quote_id\":null,\"mobile\":\"7352988879\",\"flag\":\"0\",\"policy_id\":\"328539439132919\"}', 'CURL:-> false', '2018-05-09 12:58:59', '2018-05-09 12:58:59'),
(30, '7352988879', '{\"product\":\"SBIG life\",\"premium\":null,\"email\":\"absharmca786@gmail.com\",\"expiry_date\":null,\"quote_id\":null,\"mobile\":\"7352988879\",\"flag\":\"0\",\"policy_id\":\"328539439132919\"}', '{\"status\":false,\"error_code\":400,\"error_message\":\"Message is not sent.\"}', '2018-05-09 12:58:59', '2018-05-09 12:58:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sms_details`
--
ALTER TABLE `sms_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_log`
--
ALTER TABLE `sms_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sms_details`
--
ALTER TABLE `sms_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sms_log`
--
ALTER TABLE `sms_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;COMMIT;

<?php

return [
	'policy_sms_template' => [
					'with_link'	=> [
								'a' => 'Dear Customer, thank you for renewing your policy with us. We have received an amount of :premium towards your :product_name policy number :policy_id issued on :issue_date. Link to download policy document :link'
							],
					'without_link'	=> [
								'a' => 'Dear Customer, thank you for renewing your policy with us. We have received an amount of :premium towards your :product_name policy number :policy_id issued on :issue_date.'
							]
				],
	'quote_sms_template' => [
					'with_link'	=> [
								'a' => 'Sir/Madam, your SBI General Motor Policy no. :quote_id is due for renewal on :expiry_date. Your premium is Rs. :premium. Please renew before the due date or click :link to renew now. You may visit nearest SBI/SBIG branch or call on 1800221111 for any assistance. Kindly ignore if already paid. T&C Apply'
							],
					'without_link' => [
								'a' => 'Sir/Madam, your SBI General Motor Policy no. :quote_id is due for renewal on :expiry_date. Your  premium is Rs. :premium. Please renew before the due date. You may visit nearest SBI/SBIG branch or call on 1800221111 for any assistance. Kindly ignore if already paid. T&C Apply'
							]
				],
	'OTP_sms_template' => "Dear Customer, :otp is the one time password (OTP) for your transaction on sbigeneral.in"
];

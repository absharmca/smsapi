<?php

return [
	'template_id' => [
	   'a'=> 'Dear Customer, thank you for renewing your policy with us. We have received an amount of :premium towards your :product policy number :policy_id issued on :issue_date. Link to download policy document :link',
	   'b'=> 'Dear Customer, thank you for renewing your policy with us. We have received an amount of :premium towards your :product policy number :policy_id issued on :issue_date.',
	   'c'=> 'Sir/Madam, your SBI General Motor Policy no. :quote_id is due for renewal on :expiry_date. Your premium is Rs. :premium. Please renew before the due date or click :link to renew now. You may visit nearest SBI/SBIG branch or call on 1800221111 for any assistance. Kindly ignore if already paid. T&C Apply',
           'd'=> 'Sir/Madam, your SBI General Motor Policy no. :quote_id is due for renewal on :expiry_date. Your  premium is Rs. :premium. Please renew before the due date. You may visit nearest SBI/SBIG branch or call on 1800221111 for any assistance. Kindly ignore if already paid. T&C Apply',
	   'e'=> 'testing sms template',
	],
	'OTP_sms_template' => "Dear Customer, :otp is the one time password (OTP) for your transaction on sbigeneral.in"
];
